<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|string',
            'article' => 'required',
            'cover' => 'required|image',
        ];
    }

    public function messages(){
        return [
            'title.required' => "Inserisci il titolo dell'articolo.",
            'article.required' => "Inserisci l'articolo.",
            'cover.required' => "Inserisci l'immagine dell'articolo.",
        ];
    }
}
