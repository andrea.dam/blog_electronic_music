<?php

namespace App\Http\Controllers;

use App\Models\Daw;
use App\Models\Synth;
use Illuminate\Http\Request;
use App\Http\Requests\SynthRequest;
use Illuminate\Support\Facades\Auth;

class SynthController extends Controller
{   
    public function __construct() { // Tutte le funzioni collegate a questo constroller saranno disponibili solo agli utenti loggati, a meno che non inseriamo delle eccezioni
        $this->middleware('auth')->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $synths = Synth::all();

        return view ('synth.index', compact('synths'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $daws = Daw::all();

        return view('synth.create', compact('daws'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SynthRequest $request)
    {
        // $daw = Daw::find($request->daw);
        
        // $daw->synths()->create([
        //     'name' => $request->name,
        //     'company' => $request->company,
        //     'description' => $request->description,
        //     'cover' => $request->file('cover')->store('public/synth-covers'),
        //     'user_id' => Auth::user()->id,
        // ]);

        $synth = Synth::create([
            'name' => $request->name,
            'company' => $request->company,
            'description' => $request->description,
            'cover' => $request->file('cover')->store('public/synth-covers'),
            'user_id' => Auth::user()->id,
        ]);

        foreach($request->daws as $daw) {
            $synth->daws()->attach($daw);
        }

        return redirect(route('synth.index'))->with('synthCreated', 'Hai inserito con successo un nuovo Synth');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Synth  $synth
     * @return \Illuminate\Http\Response
     */
    public function show(Synth $synth)
    {
        return view('synth.show', compact('synth'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Synth  $synth
     * @return \Illuminate\Http\Response
     */
    public function edit(Synth $synth)
    {
        $daws = Daw::all();
        return view('synth.edit', compact('synth', 'daws'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Synth  $synth
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Synth $synth)
    {
        $synth->update([
            'name' => $request->name,
            'company' => $request->company,
            'description' => $request->description,
        ]);
        
        if ($request->cover) {
            $synth->update([
                'cover' => $request->file('cover')->store('public/synth-covers'),
            ]);
        }

        //Entra nell'oggetto di classe synth, attraverso la sua relazione crea un record nella tabella pivot con l'id della daw che sto passando
        $synth->daws()->attach($request->daw);

        return redirect(route('synth.index'))->with('synthUpdated', 'Hai modificato correttamente il synth.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Synth  $synth
     * @return \Illuminate\Http\Response
     */
    public function destroy(Synth $synth)
    {
        foreach($synth->daws as $daw) {
            $synth->daws()->detach($daw);
        }
        $synth->delete();

        return redirect(route('synth.index'))->with('consoleDeleted', 'Hai eliminato con successo un synth.');
    }
}
