<?php

namespace App\Http\Controllers;

use App\Models\Daw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $daws = Daw::all();
        return view('daw.index', compact('daws'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('daw.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $synth = Daw::create([
            'name' => $request->name,
            'company' => $request->company,
            'description' => $request->description,
            'cover' => $request->file('cover')->store('public/covers'),
            'user_id' => Auth::user()->id,
        ]);

        return redirect(route('daw.index'))->with('dawCreated', 'Hai inserito con successo una nuova Daw');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Daw  $daw
     * @return \Illuminate\Http\Response
     */
    public function show(Daw $daw)
    {
        return view('daw.show', compact('daw'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Daw  $daw
     * @return \Illuminate\Http\Response
     */
    public function edit(Daw $daw)
    {
        return view('daw.edit', compact('daw'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Daw  $daw
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Daw $daw)
    {
        $daw->update([
            'name' => $request->name,
            'company' => $request->company,
            'description' => $request->description,
        ]);
        
        if ($request->cover) {
            $daw->update([
                'cover' => $request->file('cover')->store('public/covers'),
            ]);
        }

        return redirect(route('daw.index'))->with('dawUpdated', 'Hai modificato correttamente la daw.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Daw  $daw
     * @return \Illuminate\Http\Response
     */
    public function destroy(Daw $daw)
    {
        $daw->delete();

        return redirect(route('daw.index'))->with('dawDeleted', 'Hai eliminato con successo la daw.');
    }
}
