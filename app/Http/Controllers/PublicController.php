<?php

namespace App\Http\Controllers;

use App\Models\Daw;
use App\Models\Synth;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function homepage() {
        //Query al database --> richiesta
        //All() => Select * from articles table
        $daws = Daw::orderBy('created_at', 'DESC')->take(1)->get(); // Entra nella tabella articles, riordina tutti i record in ordine discendente rispetto a created_at, prendi i primi 4, crea una collezione
        $synths = Synth::orderBy('created_at', 'DESC')->take(1)->get();
        return view('welcome', compact('daws'), compact('synths'));
    }

    public function profile() {
        return view('profile');
    }

    public function destroy() {
        $userSynths = Auth::user()->synths;

        foreach($userSynths as $userSynth) {
            $userSynth->update ([
                'user_id' => 1,
            ]);
        }

        Auth::user()->delete();

        return redirect(route('homepage'))->with('userDeleted', 'Hai cancellato tutti i tuoi dati.');
    }
}
