<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;

class ArticleController extends Controller
{
    public function create(){
        return view('article.create');
    }

    public function store(ArticleRequest $request){

        // Mass Assignment
        $article = Article::create([
            'title' => $request->title,
            'article' => $request->article,
            'cover' => $request->file('cover')->store('public/covers')
        ]);

        return redirect(route('article.index'))->with('articleCreated', 'Hai inserito con successo l\'articolo');
    }

    public function index() {
        $articles = Article::all();

        return view('article.index', compact('articles'));
    }
}
