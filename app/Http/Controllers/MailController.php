<?php

namespace App\Http\Controllers;

use Exception;
use App\Mail\AdminMail;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use App\Http\Requests\MailRequest;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function contactUs() {
        return view('contact-us');
    }
    
    public function submit(MailRequest $request) {
        
        $name = $request->name;
        $email = $request->email;
        $message = $request->message;
        
        $user_data = compact('name', 'message');
        // dd($user_data);

        try {
            Mail::to($email)->send(new ContactMail($user_data));
            Mail::to('admin@blog.it')->send(new AdminMail($user_data, $email));
        } catch (Exception $e) {
            return redirect(route('contact-us'))->with('error', 'C\'è stato un problema con l\'invio del tuo messaggio, per favore, riprova più tardi.');
        }
        
        return redirect(route('homepage'))->with('message', 'Il tuo messaggio è stato inoltrato con successo.');
    }
}
