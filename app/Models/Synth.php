<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Synth extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'company',
        'cover',
        'description',
        'user_id'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function daws() {
        return $this->belongsToMany(Daw::class);
    }
}
