<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Daw extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'company',
        'cover',
        'description',
        'user_id'
    ];
    //Otm
    public function user() {
        return $this->belongsTo(User::class);
    }

    // Mtm
    public function synths () {
        return $this->belongsToMany(Synth::class);
    }
}
