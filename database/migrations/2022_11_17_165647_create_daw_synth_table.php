<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daw_synth', function (Blueprint $table) {
            $table->id();

            //FK Daw
            $table->unsignedBigInteger('daw_id');
            $table->foreign('daw_id')->references('id')->on('daws');
            //FK Synth
            $table->unsignedBigInteger('synth_id');
            $table->foreign('synth_id')->references('id')->on('synths');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daw_synth');
    }
};
