<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1 id="heading">Hai ricevuto un nuovo messaggio da {{$user_data['name']}}</h1>
    <h2 id="heading">Mail mittente: {{$email}}</h2>
    <h3 id="heading">{{$user_data['name']}} ti ha scritto:</h3>
    <p>{{$user_data['message']}}</p>
</body>
</html>