<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Grazie per averci contattato</title>
</head>
<body>
    <h1 id="heading">Ciao {{$user_data['name']}}, grazie per averci contattato!</h1>
    <h2 id="heading">Ti contatteremo entro le prossime 48 ore.</h2>
    <p>Il tuo messaggio è:</p>
    <p>{{$user_data['message']}}</p>
</body>
</html>