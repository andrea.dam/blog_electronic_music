<x-layout>
    <x-title>Contattaci</x-title>
    <x-header>Contattaci</x-header>
    <main class="container">
        <div class="row justify-content-center">
            
            {{-- Se uno o più dati non sono stati inseriti correttamente --}}
            @if ($errors->any())
            <div class="col-12 col-md-8 alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            {{-- Se l'invio della mail non è andato a buon fine per cause esterne al nostro sito --}}
            @if (session('error'))
            <div class="col-12 col-md-8 alert alert-danger">
                <p class="text-danger">{{session('error')}}</p>
            </div>
            @endif
            
            <div class="col-12 col-md-8">
                <form method="POST" action="{{route('contact-us.submit')}}">
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">Nome</label>
                        <input type="text"" class="form-control" id="name" name="name">
                    </div>
                    <div class="mb-3">
                        <label for="email" class="form-label">Indirizzo Email</label>
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                    <label for="message" class="form-label">Messaggio</label>
                    <textarea class="form-control" name="message" id="message" cols="30" rows="7"></textarea>
                    <div class="d-flex justify-content-between mt-3">
                        <button type="submit" class="btn btn-success p-3 me-3">Invia</button>
                        <a href="{{route('homepage')}}" role="button" class="btn btn-secondary p-3">Torna Indietro</a>
                    </div>
                </form>
            </div>
        </div>
    </main>
</x-layout>