<x-layout>
    
    <x-title>Tutti le Daw</x-title>
    
    <x-header>
        Tutti le Daw
    </x-header>
    
    <main class="container mb-5">
        @if (session('dawCreated'))
        <div class="col alert alert-success">
            {{session('dawCreated')}}
        </div>
        @endif
        <div class="row row-cols-md-4 justify-content-around">
            @if (count($daws) > 0)
            @foreach ($daws as $daw)
            <div class="col border p-4 rounded">
                <img class="img-fluid" src="{{Storage::url($daw->cover)}}" alt="">
                <div class="d-flex justify-content-between align-items-center">
                    <h2 id="heading" class="my-4 text-light">{{$daw->company}} {{$daw->name}}</h2>
                    <a role="button" class="btn btn-light" href="{{route('daw.show', compact('daw'))}}">Continua a leggere</a>
                </div>
                <p class="text-light">{{$daw->description}}</p>
            </div>
            @endforeach
            @else
            <h2 id="heading">Non sono ancora state inserite Daw</h2>
            @endif
            
        </div>
    </main>
    
</x-layout>