<x-layout>
    
    <x-title>{{$daw->company}} {{$daw->name}}</x-title>
    <x-header>{{$daw->company}} {{$daw->name}}</x-header>

    <main class="container">
        <section class="row justify-content-center">
            <div class="col-12 col-md-8 border-end border-start px-4 mb-3">
                <img src="{{Storage::url($daw->cover)}}" alt="" class="img-fluid rounded mb-4">
                <p class="mb-0">{{$daw->description}}</p>
                <h3 class="mt-3">Synth disponibili:</h3>
                @if (count($daw->synths) > 0)
                <ul>
                    @foreach ($daw->synths as $synth)
                    <li>{{$synth->company}} {{$synth->name}}</li>
                    @endforeach
                </ul>
                @endif
                <p class="mt-3">Scritto da: {{$daw->user->name}} - {{$daw->user->email}}</p>
            </div>
            <div class="col-12 col-md-8 my-4 d-flex justify-content-between">
                <a href="{{route('homepage')}}" role="button" class="btn btn-light p-4">Torna alla Home</a>
                @if (Auth::user() && Auth::user()->id == $daw->user_id)
                <div class="d-flex">
                    <a href="{{route('daw.edit', compact('daw'))}}" role="button" class="btn btn-warning p-4 me-3">Modifica</a>
                    <form method="POST" action="{{route('daw.destroy', compact('daw'))}}">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger p-4">Elimina</button>
                    </form>
                </div>
                @endif
            </div>
        </section>
    </main>
    
</x-layout>