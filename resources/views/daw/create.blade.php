<x-layout>
    <x-title>H56 Aggiungi Daw</x-title>
    <x-header>Aggiungi Daw</x-header>
    <main class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{route('daw.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="name" class="form-label">Nome Daw</label>
                        <input name="name" type="text" class="form-control" id="name" type="text" value="{{old('name')}}">
                    </div>
                    <div class="mb-3">
                        <label for="company" class="form-label">Brand</label>
                        <input name="company" type="text" class="form-control" id="company" type="text" value="{{old('company')}}">
                    </div>
                    <div class="mb-3">
                        <label for="description" class="form-label">Descrizione</label>
                        <textarea class="form-control" name="description" id="description" type="text" rows="8">{{old('description')}}</textarea>
                    </div>
                    <div class="mb-4">
                        <label for="cover" class="form-label">Immagine</label>
                        <input class="form-control" name="cover" id="cover" type="file"></input>
                    </div>
                    <div class="d-flex justify-content-between">
                        <button type="submit" class="btn btn-success p-3 me-3">Inserisci Daw</button>
                        <a href="{{route('homepage')}}" role="button" class="btn btn-secondary p-3">Torna Indietro</a>
                    </div>
                </form>
            </div>
        </div>
    </main>
</x-layout>