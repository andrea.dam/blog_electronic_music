<x-layout>
    
    <x-title>Tutti i Software Synths</x-title>
    
    <x-header>
        Tutti i Software Synths
    </x-header>
    
    <main class="container mb-5">
        <div class="row row-cols-md-4 justify-content-around">
            @if (session('synthCreated'))
            <div class="col alert alert-success">
                {{session('synthCreated')}}
            </div>
            @endif
            @if (count($synths) > 0)
            @foreach ($synths as $synth)
            <div class="col border p-4 rounded">
                <img class="img-fluid" src="{{Storage::url($synth->cover)}}" alt="">
                <div class="d-flex justify-content-between align-items-center">
                    <h2 id="heading" class="my-4 text-light">{{$synth->company}} {{$synth->name}}</h2>
                    <a role="button" class="btn btn-light" href="{{route('synth.show', compact('synth'))}}">Continua a leggere</a>
                </div>
                <p class="text-light">{{$synth->description}}</p>
            </div>
            @endforeach
            @else
            <h2 id="heading">Non sono ancora stati inseriti Plugin</h2>
            @endif
            
        </div>
    </main>
    
</x-layout>