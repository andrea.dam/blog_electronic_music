<x-layout>
    
    <x-title>{{$synth->company}} {{$synth->name}}</x-title>
    <x-header>{{$synth->company}} {{$synth->name}}</x-header>
    
    <main class="container">
        <section class="row justify-content-center">
            <div class="col-12 col-md-8 border-end border-start px-4 mb-3">
                <img src="{{Storage::url($synth->cover)}}" alt="" class="img-fluid rounded mb-4">
                <p class="mb-0">{{$synth->description}}</p>
                <h3 class="mt-3">Daw disponibili:</h3>
                @if (count($synth->daws) > 0)
                <ul>
                    @foreach ($synth->daws as $daw)
                    <li>{{$daw->company}} {{$daw->name}}</li>
                    @endforeach
                </ul>
                @endif
                <p class="mt-3">Scritto da: {{$synth->user->name}} - {{$synth->user->email}}</p>
            </div>
            <div class="col-12 col-md-8 my-4 d-flex justify-content-between">
                <a href="{{route('homepage')}}" role="button" class="btn btn-light p-4">Torna alla Home</a>
                @if (Auth::user() && Auth::user()->id == $synth->user_id)
                <div class="d-flex">
                    <a href="{{route('synth.edit', compact('synth'))}}" role="button" class="btn btn-warning p-4 me-3">Modifica</a>
                    <form method="POST" action="{{route('synth.destroy', compact('synth'))}}">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger p-4">Elimina</button>
                    </form>
                </div>
                @endif
            </div>
        </section>
    </main>
    
</x-layout>