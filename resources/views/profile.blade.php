<x-layout>
    
    <x-title>{{Auth::user()->name}}</x-title>
    <x-header>Benvenuto, {{Auth::user()->name}}</x-header>
    
    <main class="container">
        <h2>Hai creato questi articoli:</h2>
        <section class="row justify-content-start">
            {{-- <div class="row row-cols-2 justify-content-center"> --}}
                @foreach (Auth::user()->synths as $synth)
                <div class="col border p-4 rounded">
                    <img class="img-fluid" src="{{Storage::url($synth->cover)}}" alt="">
                    <div class="d-flex justify-content-between align-items-center">
                        <h2 id="heading" class="my-4 text-light">{{$synth->company}} {{$synth->name}}</h2>
                        <a role="button" class="btn btn-light" href="{{route('synth.show', compact('synth'))}}">Continua a leggere</a>
                    </div>
                    <p class="text-light">{{$synth->description}}</p>
                </div>
                @endforeach
                {{-- </div> --}}
            </section>
            
        </main>
        <section class="container text-center mt-5">

            <div class="row justify-content-center mb-3">
                <form class="col" method="POST" action="{{route('user.destroy')}}">
                    @csrf
                    @method('delete')
                    <button class="btn btn-danger p-4" type="submit">Elimina utente</button>
                </form>
            </div>
        </section>
            
</x-layout>