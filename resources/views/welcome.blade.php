<x-layout>
    
    <x-title>H56 Audio Plugins</x-title>
    <header class="container-fluid d-flex justify-content-center align-items-center text-light mb-5" id="homepage-header">
        <h1 id="heading" class="display-1">H56 Audio Plugins</h1>
    </header>
    
    <main class="container">
        
        {{-- Se è stata inviata una mail --}}
        @if (session('message'))
        <div class="alert alert-success">
            {{session('message')}}
        </div>
        @endif
        <section class="row mb-5">
            {{-- L'ultimo plugin --}}
            @if (count($daws) > 0)
            <div class="col-12 col-md-6">
                <h2 id="heading" class="mb-4 text-end text-light">L'ultima Daw inserita</h2>
                @foreach ($daws as $daw)
                <div class="border p-4 rounded">
                    <img class="img-fluid" src="{{Storage::url($daw->cover)}}" alt="">
                    <div class="d-flex justify-content-between align-items-center">
                        <h2 id="heading" class="my-4 text-light">{{$daw->name}}</h2>
                        <a role="button" class="btn btn-light" href="{{route('daw.show', compact('daw'))}}">Continua a leggere</a>
                    </div>
                    <p class="text-light">{{$daw->description}}</p>
                </div>
                @endforeach
                <div class="col-12 col-md-6"></div>
            </div>
            @else
            @endif
            {{-- L'ultimo synth --}}
            @if (count($synths) > 0)
            <div class="col-12 col-md-6">
                <h2 id="heading" class="mb-4 text-light">L'ultimo Synth inserito</h2>
                @foreach ($synths as $synth)
                <div class="border p-4 rounded">
                    <img class="img-fluid" src="{{Storage::url($synth->cover)}}" alt="">
                    <div class="d-flex justify-content-between align-items-center">
                        <h2 id="heading" class="my-4 text-light">{{$synth->company}} {{$synth->name}}</h2>
                        <a role="button" class="btn btn-light" href="{{route('synth.show', compact('synth'))}}">Continua a leggere</a>
                    </div>
                    <p class="text-light">{{$synth->description}}</p>
                </div>
                @endforeach
                <div class="col-12 col-md-6"></div>
            </div>
            @else
            @endif
        </section>
        
        <section class="row mb-5">
            <div class="col-12 col-md-6 text-end">
                <h2 id="heading" class="mb-3 text-light">Sfoglia le Daw</h2>
                <a href="{{route('daw.index')}}" role="button" class="btn btn-light p-4">Tutte le Daw</a>
            </div>
            <div class="col-12 col-md-6">
                <h2 id="heading" class="mb-3 text-light">Sfoglia i Synth</h2>
                <a href="{{route('synth.index')}}" role="button" class="btn btn-light p-4">Tutti i Synth</a>
            </div>
        </section>
    </main>
    
</x-layout>