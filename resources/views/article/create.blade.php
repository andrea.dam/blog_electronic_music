<x-layout>
    <x-title>H56 Aggiungi un Plugin</x-title>
    <x-header>Aggiungi Plugin</x-header>
    <main class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-8">
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{route('article.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="title" class="form-label">Titolo Articolo</label>
                        <input name="title" type="text" class="form-control" id="title" type="" value="{{old('title')}}">
                    </div>
                    <div class="mb-3">
                        <label for="article" class="form-label">Articolo</label>
                        <textarea class="form-control" name="article" id="article" type="text" rows="8">{{old('article')}}</textarea>
                    </div>
                    <div class="mb-4">
                        <label for="cover" class="form-label">Immagine</label>
                        <input class="form-control" name="cover" id="cover" type="file"></input>
                    </div>
                    <div class="d-flex justify-content-between">
                        <button type="submit" class="btn btn-success p-3 me-3">Inserisci Articolo</button>
                        <a href="{{route('homepage')}}" role="button" class="btn btn-secondary p-3">Torna Indietro</a>
                    </div>
                </form>
            </div>
        </div>
    </main>
</x-layout>