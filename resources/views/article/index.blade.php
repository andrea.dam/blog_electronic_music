<x-layout>
    
    <x-header>
        Tutti gli Articoli
    </x-header>
    
    <main class="container mb-5">
        <div class="row row-cols-2">
            {{-- Se è stato creato un articolo --}}
            @if (session('articleCreated'))
            <div class="col-12 alert alert-success">
                {{session('articleCreated')}}
            </div>
            @endif
            @foreach ($articles as $article)
            <div class="col border p-4 rounded">
                <img class="img-fluid" src="{{Storage::url($article->cover)}}" alt="">
                <h2 id="heading" class="my-4">{{$article->title}}</h2>
                <p>{{$article->article}}</p>
            </div>
            @endforeach
            
        </div>
    </main>
    
</x-layout>