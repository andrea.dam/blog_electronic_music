<nav id="navbar" class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand ms-2" href="{{route('homepage')}}">H56</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link {{Route::is('homepage') ? 'active' : ''}}" href="{{route('homepage')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{Route::is('contact-us') ? 'active' : ''}}" href="{{route('contact-us')}}">Contattaci</a>
                </li>
                @auth
                <li class="nav-item">
                    <a class="nav-link {{Route::is('daw.create') ? 'active' : ''}}" href="{{route('daw.create')}}">Inserisci una Daw</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{Route::is('synth.create') ? 'active' : ''}}" href="{{route('synth.create')}}">Inserisci un Synth</a>
                </li>
                @endauth
            </ul>
            <ul class="navbar-nav mb-2 mb-lg-0 me-2">
                {{-- Per utenti loggati --}}
                @auth
                <li class="nav-item">
                    <a class="nav-link" href="#">Ciao {{Auth::user()->name}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('profile')}}"><i class="fa-solid fa-user text-secondary "></i></a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="#" onclick="event.preventDefault(); document.querySelector('#form-logout').submit();">Logout</a>
                    <form id="form-logout" action="{{route('logout')}}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
                @else
                {{-- Per utenti Guest --}}
                <li class="nav-item">
                    <a class="nav-link" href="{{route('login')}}">Accedi</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('register')}}">Registrati</a>
                </li>
                @endauth
                {{-- <li class="nav-item">
                    <a id="btn-light" class="nav-link d-inline-block" role="button" href=""><i class="fa-solid fa-sun text-secondary"></i></a>
                    <a id="btn-dark" class="nav-link d-inline-block" role="button" href=""><i class="fa-solid fa-moon text-secondary"></i></a>
                </li> --}}
            </ul>
        </div>
    </div>
</nav>