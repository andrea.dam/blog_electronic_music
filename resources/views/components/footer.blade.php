<footer class="pt-5 border-top mt-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-3 mb-5">
                <a class="d-inline-block mb-4" href="#">
                    <img class="img-fluid" src="bootstrap5-plain-assets/logos/plainb-logo.svg" alt="" width="96px">
                </a>
                <p class="text-muted mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                <div>
                    <a class="d-inline-block mx-4" href="#">
                        <i class="fa-brands fa-facebook text-secondary"></i>
                    </a>
                    <a class="d-inline-block me-4" href="#">
                        <i class="fa-brands fa-github text-secondary"></i>
                    </a>
                    <a class="d-inline-block me-4" href="#">
                        <i class="fa-brands fa-instagram text-secondary"></i>
                    </a>
                    <a class="d-inline-block me-4" href="#">
                        <i class="fa-brands fa-linkedin text-secondary"></i>
                    </a>
                    <a class="d-inline-block" href="#">
                        <i class="fa-brands fa-twitter text-secondary"></i>
                    </a>
                </div>
            </div>
            <div class="col-12 col-lg-9">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-3 mb-5">
                        <h4 id="heading" class="mb-4 text-light">Company</h4>
                        <ul class="list-unstyled">
                            <li class="mb-2"><a class="text-muted" href="#">About Us</a></li>
                            <li class="mb-2"><a class="text-muted" href="#">Careers</a></li>
                            <li class="mb-2"><a class="text-muted" href="#">Press</a></li>
                            <li><a class="text-muted" href="#">Blog</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 mb-5">
                        <h4 id="heading" class="mb-4 text-light">Pages</h4>
                        <ul class="list-unstyled">
                            <li class="mb-2"><a class="text-muted" href="#">Login</a></li>
                            <li class="mb-2"><a class="text-muted" href="#">Register</a></li>
                            <li class="mb-2"><a class="text-muted" href="#">Add list</a></li>
                            <li><a class="text-muted" href="#">Contact</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 mb-5">
                        <h4 id="heading" class="mb-4 text-light">Legal</h4>
                        <ul class="list-unstyled">
                            <li class="mb-2"><a class="text-muted" href="#">Terms</a></li>
                            <li class="mb-2"><a class="text-muted" href="#">About Us</a></li>
                            <li class="mb-2"><a class="text-muted" href="#">Team</a></li>
                            <li><a class="text-muted" href="#">Privacy</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-6 col-lg-3 mb-5">
                        <h4 id="heading" class="mb-4 text-light">Resources</h4>
                        <ul class="list-unstyled">
                            <li class="mb-2"><a class="text-muted" href="#">Blog</a></li>
                            <li class="mb-2"><a class="text-muted" href="#">Service</a></li>
                            <li class="mb-2"><a class="text-muted" href="#">Product</a></li>
                            <li><a class="text-muted" href="#">Pricing</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>