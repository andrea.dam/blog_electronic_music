<header class="container-fluid text-center my-5">
    <div class="row">
        <div class="col-12">
            <h1 id="heading" class="display-1">{{$slot}}</h1>
        </div>
    </div>
</header>