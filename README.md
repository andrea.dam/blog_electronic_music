# Blog Personale (Music Plugins)

Blog realizzato come esercizio darente il corso Hackademy di Aulab.

Scopo principale della realizzazione del blog era quello di creare la logica delle relazioni database fra i diversi modelli. Nel mio caso ho scelto di utilizzare come modelli le DAW (Digital Audio Workstation, aka i software che si usano per fare musica) e i plugin che si usano al loro interno.

La relazione fra utente e DAW/Plugin è 1 a N, mentre quella tra DAW e Plugins è di N a N.
