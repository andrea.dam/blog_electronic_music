<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MailController;
use App\Http\Controllers\SynthController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\DawController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'homepage'])->name('homepage');
Route::get('/user/profile', [PublicController::class, 'profile'])->name('profile');
Route::delete('/user/destroy', [PublicController::class, 'destroy'])->name('user.destroy');

// Rotte Contattaci
Route::get('/contact-us', [MailController::class, 'contactUs'])->name('contact-us');
Route::post('/contact-us', [MailController::class, 'submit'])->name('contact-us.submit');

// Rotte Synth
Route::get('/synth/index', [SynthController::class, 'index'])->name('synth.index');
Route::get('/synth/create', [SynthController::class, 'create'])->name('synth.create');
Route::post('/synth/store', [SynthController::class, 'store'])->name('synth.store');
Route::get('/synth/show/{synth}', [SynthController::class, 'show'])->name('synth.show');
Route::get('/synth/edit/{synth}', [SynthController::class, 'edit'])->name('synth.edit');
Route::put('/synth/update/{synth}', [SynthController::class, 'update'])->name('synth.update');
Route::delete('/synth/destroy/{synth}', [SynthController::class, 'destroy'])->name('synth.destroy');

// Rotte Daw
Route::get('/daw/index', [DawController::class, 'index'])->name('daw.index');
Route::get('/daw/create', [DawController::class, 'create'])->name('daw.create');
Route::post('/daw/store', [DawController::class, 'store'])->name('daw.store');
Route::get('/daw/show/{daw}', [DawController::class, 'show'])->name('daw.show');
Route::get('/daw/edit/{daw}', [DawController::class, 'edit'])->name('daw.edit');
Route::put('/daw/update/{daw}', [DawController::class, 'update'])->name('daw.update');
Route::delete('/daw/destroy/{daw}', [DawController::class, 'destroy'])->name('daw.destroy');